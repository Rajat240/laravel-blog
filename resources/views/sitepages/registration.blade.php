@extends('app')
@section('content')

<section id="banner">
 
    <div class="page_text">
        <div class="container">
            <div class="section-title about">
                <h3 class="title" data-aos="fade-right"  data-aos-delay="500">
                    Register
                </h3>

            </div>					
        </div>
    </div>
</section>
<!-- Intro Section -->

<section id="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                @if (count($errors) > 0)					
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h4 class="text-danger"><i class="fa fa-exclamation-triangle"></i> Error!</h4>
            <ul class="mb-0 px-0 list-style-none">
                @foreach ($errors->all() as $error)
                <li><i class="fa fa-chevron-right"></i> {{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('flash_message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h4 class="text-success mb-0"><i class="fa fa-check-circle"></i> {{ Session::get('flash_message') }} </h4>
        </div>
        @endif
                <div class="login_page">
                    {!! Form::open(array('url'=>'registration', 'class'=>'padding-15','role','enctype' => 'multipart/form-data' )) !!}
 
<!--                    <form action="/action_page.php">-->
                        <div class="form-group">
                            <label>Enter your username</label>
                            <input type="text" class="form-control fome_career" id="uname" name="uname">
                        </div>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Enter your email</label>
                                    <input type="text" class="form-control fome_career" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label>Enter your password</label>
                            <input type="password" name="password" class="form-control fome_career" id="password">
                        </div>

                        <div class="form-group">
                            <label>Confirm password</label>
                            <input type="password" name="cpassword" class="form-control fome_career" id="cpassword">
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">						
                                    <input type="text" class="form-control fome_career" id="first_name" placeholder="First name" name="first_name">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">									
                                    <input type="text" class="form-control fome_career" id="last_name"  placeholder="Last name" name="last_name">
                                </div>
                            </div>
                        </div>
<!--                    </form>-->
<!--                    <a href="#" class="btn btn_login">CREATE ACCOUNT</a>-->
        <button type="submit"  class="btn btn_login">CREATE ACCOUNT</button>
                {!! Form::close() !!}
                  
                    
               
                </div>
            </div>
           
        </div>	
    </div>
</section>

<!--<script>
    function Register(){
        
        var _token = $('#_token').val();
        var uname = $('#uname').val();
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var email = $('#email').val();
        var password = $('#password').val();
       // var cpassword = $('#cpassword').val();
        
        $.post('/postregistration', {_token:_token,uname:uname,first_name:first_name,last_name:last_name,email:email,password:password},function(result){
            
           if(result == 1)
       {
        $('.success').css('display','block');
        $('#uname').val('');
        $('#first_name').val('');
        $('#last_name').val('');
        $('#email').val('');
        $('#password').val('');
        $('#cpassword').val('');
        $('.success').html('Your Registration Has Been Done Successfully!');
        setTimeout(function() {
                $('.success').fadeOut('fast');
            }, 5000);
         
       }else{
           $('.success').css('display','block');
           $('#uname').val('');
          $('#first_name').val('');
          $('#last_name').val('');
          $('#email').val('');
          $('#password').val('');
          $('#cpassword').val('');
           $('.error').html('This email ID is already Used Try to another email.');
            setTimeout(function() {
                $('.error').fadeOut('fast');
            }, 5000);
       }
        });
    }
</script>-->
@endsection