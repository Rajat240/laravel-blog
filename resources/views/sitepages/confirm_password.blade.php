@extends('app')
@section('content')

<section id="banner">
     
    <div class="page_text">
        <div class="container">
            <div class="section-title about">
                <h3 class="title" data-aos="fade-right"  data-aos-delay="500">
                    Change Password
                </h3>

            </div>					
        </div>
    </div>
</section>
<!-- Intro Section -->

<section id="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                 @if (count($errors) > 0)					
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h4 class="text-danger"><i class="fa fa-exclamation-triangle"></i> Error!</h4>
            <ul class="mb-0 px-0 list-style-none">
                @foreach ($errors->all() as $error)
                <li><i class="fa fa-chevron-right"></i> {{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('flash_message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h4 class="text-success mb-0"><i class="fa fa-check-circle"></i> {{ Session::get('flash_message') }} </h4>
        </div>
        @endif
                <div class="login_page">
                    {!! Form::open(array('url'=>'resetpassword','id'=>'login','class'=>'padding-15','role','enctype' => 'multipart/form-data' )) !!}
                    <input type="hidden" name="email" value="{{$username}}">
                    <input type="hidden" name="code" value="{{$code}}">
                        <div class="form-group">
                            <label>Enter New Password</label>
                            <input type="password" class="form-control fome_career" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label>Enter Confirm Password</label>
                            <input type="password" class="form-control fome_career" id="confirm" name="confirm">
                        </div>
                    <button type="submit" class="btn btn_login">Change Password</button>
                    
                    {!! Form::close() !!}
                </div>
            </div>
<!--            <div class="col-lg-6 text-lg-right text-center">
                <a href="{{URL::to('registration')}}" class="btn btn-theme">Sign Up</a>
                <img src="{{URL::asset('site_assets/images/x1.png')}}" class="w-100"/>
            </div>-->
        </div>	
    </div>
</section>

@endsection